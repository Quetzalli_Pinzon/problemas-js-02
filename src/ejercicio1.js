var personArr = [
    {
        "personId": 123, 
        "name": "Jhon", 
        "city": "Melbourne", 
        "phoneNo": "1234567890"
    },
    {
        "personId": 124, 
        "name": "Amelia" , 
        "city": "Sydney", 
        "phoneNo": "1234567890"
    }, 
    {
        "personId": 125,
        "name": "Emily", 
        "city": "Perth", 
        "phoneNo": "1234567890"
    },
    {
        "personId": 126, 
        "name": "Abraham",
        "city": "Perth", 
        "phoneNo": "1234567890"}
    ];

let crearTabla = function(){
    let stringTabla = "<tr><th>ID</th><th>Name</th><th>City</th><th>Phone number</th></tr>";
    let body = '';
    for (let i = 0; i < personArr.length; i++){
        body += `<tr><td>${personArr[i].personId}</td><td>${personArr[i].name}</td><td>${personArr[i].city}</td><td>${personArr[i].phoneNo}</td></tr>`;
    }
    stringTabla += body;
    return stringTabla;
}

document.getElementById('tabla-id').innerHTML = crearTabla() ;
